from sdpw.ellipse import Ellipse
from sdpw.sdpw import pw_contact
from pytest import approx
import numpy as np
import pytest


# tests for the regular Perram-Wertheim. really just for the
# Newton-Raphson iteration ...

@pytest.fixture
def A():
    return Ellipse(1, 1, 0)


@pytest.fixture
def B():
    return Ellipse(1, 0, 0)


# non-degenerate arguments

def test_circles(A):
    rAB = np.array([2, 0])
    res = pw_contact(A, A, rAB)
    assert res.maximum == approx(1.0)
    assert res.maximiser == approx(0.5)


# when called with a degenerate argument it may succeed ...

def test_circle_line1(A, B):
    rAB = np.array([2, 0])
    res = pw_contact(A, B, rAB)
    assert res.maximum == approx(1.0)


# .. but may fail

def test_circle_line2(A, B):
    rAB = np.array([0, 1])
    with pytest.raises(RuntimeError):
        pw_contact(A, B, rAB)
