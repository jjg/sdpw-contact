from sdpw.ellipse import Ellipse
from sdpw.sdpw import ddfAB, ddfAB0
from pytest import approx
import numpy as np
import pytest


# ddfAB(x) -> ddfAB0 as x -> 0, initial version of this test
# had that B was a circle, in this case ddfAB(x) seems to be
# a constant; i.e., fAB is a quadratic -- can you prove that?

@pytest.fixture
def A():
    return Ellipse(1, 0, 0.2)


@pytest.fixture
def B():
    return Ellipse(1, 0.5, 0.0)


def test_limit_formula(A, B):
    xs = [0.1, 0.01, 0.001, 0.0001, 0.0001]
    rAB = np.array([1, 0])
    y0 = ddfAB0(A, B, rAB)
    ys = [ddfAB(A, B, rAB, x) for x in xs]
    errs = [abs(y - y0) for y in ys]
    assert all([err < x for x, err in zip(xs, errs)])
