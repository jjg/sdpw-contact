from sdpw.ellipse import Ellipse
from sdpw.sdpw import fAB, fAB0, dfAB, dfAB0
from pytest import approx
import numpy as np
import pytest


@pytest.fixture
def A():
    return Ellipse(1, 0, 0.1)


@pytest.fixture
def B():
    return Ellipse(1, 0.5, 0.0)


# (fAB(A, B, rAB, x) - fAB0(A, B, rAB)) / x -> dfAB0(A, B, rAB)

def test_limit_direct(A, B):
    xs = [0.1, 0.01, 0.001, 0.0001]
    rAB = np.array([1, 0])
    y0 = fAB0(A, B, rAB)
    dy0 = dfAB0(A, B, rAB)
    dys = [(fAB(A, B, rAB, x) - y0) / x for x in xs]
    errs = [abs(dy - dy0) for dy in dys]
    assert all([err < x for x, err in zip(xs, errs)])


# dfAB(A, B, rAB, x) -> dfAB0(A, B, rAB)

def test_limit_formula(A, B):
    xs = [0.1, 0.01, 0.001, 0.0001]
    rAB = np.array([1, 0])
    y0 = dfAB0(A, B, rAB)
    ys = [dfAB(A, B, rAB, x) for x in xs]
    errs = [abs(y - y0) for y in ys]
    assert all([err < 2 * x for x, err in zip(xs, errs)])
