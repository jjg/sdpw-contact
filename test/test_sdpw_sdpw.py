from sdpw.ellipse import Ellipse
from sdpw.sdpw import sdpw_contact
from pytest import approx
import numpy as np
import pytest


@pytest.fixture
def A():
    return Ellipse(1, 0, 0)


@pytest.fixture
def B():
    return Ellipse(1, 1, 0)


def assert_angle_approx(t1, t2):
    assert np.sin(t1) == approx(np.sin(t2))
    assert np.cos(t1) == approx(np.cos(t2))


# a circle with a line (in the radial direction) touching it

def test_circle_radial(A, B):
    rAB = np.array([2, 0])
    res = sdpw_contact(A, B, rAB)
    assert res.maximum == approx(1)
    assert_angle_approx(res.theta, 0)


# likewise, but reflected in the y-axis

def test_circle_radial_reflected(A, B):
    rAB = np.array([-2, 0])
    res = sdpw_contact(A, B, rAB)
    assert res.maximum == approx(1)
    assert_angle_approx(res.theta, np.pi)


# a circle with a line whose midpoint is tangent to it

def test_circle_tangent_mid(A, B):
    rAB = np.array([0, 1])
    res = sdpw_contact(A, B, rAB)
    assert res.maximum == approx(1)
    assert_angle_approx(res.theta, np.pi / 2)


# likewise, but reflected in the x-axis

def test_circle_tangent_mid_reflected(A, B):
    rAB = np.array([0, -1])
    res = sdpw_contact(A, B, rAB)
    assert res.maximum == approx(1)
    assert_angle_approx(res.theta, -np.pi / 2)


# a circle with a line whose endpoint is tangent to it

def test_circle_tangent_end(A, B):
    rAB = np.array([1, 1])
    res = sdpw_contact(A, B, rAB)
    assert res.maximum == approx(1)
    assert_angle_approx(res.theta, np.pi / 2)


# a circle with a line whose extension is tangent to it, but
# does not actually touch it (so contact value is > 1)

def test_circle_tangent_extended(A, B):
    rAB = np.array([2, 1])
    res = sdpw_contact(A, B, rAB)
    assert res.maximum > 1
    assert res.theta < np.pi / 2
    assert res.theta > 0


# half way between radial and end-point tangent

def test_circle_skew_end(A, B):
    t = 1/np.sqrt(2)
    rAB = np.array([1+t, t])
    res = sdpw_contact(A, B, rAB)
    assert res.maximum == approx(1)
    assert_angle_approx(res.theta, np.pi / 4)


# quadratic dependence on rAB when B is a circle

def test_quadratic_circle(A, B):
    for t in np.linspace(0.5, 4.5, 81):
        rAB = np.array([t, 0])
        res = sdpw_contact(A, B, rAB)
        assert res.maximum == approx((t/2)**2)
        assert res.theta == approx(0)


# regression
#
# This noticed in the 'contour' plot, which seemed to have
# a discontinuity in the principal direction of the degenerate
# ellipse, but only when the non-degenerate ellipse is a
# proper ellipse (see above) and at an angle. The discontinuity
# seems to be when t ~ 1.5 when the contact function drops from
# 1.21 to 1.05, we also see some inaccuracy just before the drop
# at t ~ 1.  This turned out to be due to the optimisation of
# stopping the iteration as soon as the objective was greater
# than one, which is good for efficiency, but bad for plotting.
#
# When B is a proper ellipse, the it should be quadratic with
# respect to rAB

def test_quadratic_ellipse():
    A = Ellipse(1, 0, 0)
    B = Ellipse(0.5, 0.25, np.pi/5)
    K = sdpw_contact(A, B, np.array([1, 0])).maximum
    for t in np.linspace(0.5, 2.5, num=80, endpoint=True):
        rAB = np.array([t, 0])
        res = sdpw_contact(A, B, rAB)
        assert res.maximum == approx(K * t**2)


# regression
#
# Small line-segment with a larger ellipse.
#
# We had a function fAB passed to the Scipy brent optimiser,
# but that went beserk, escaped the bracket and ran off to
# infinity -- the issue being that the behaviour of fAB
# outside [0, 1] was undesigned (since uninteresting) but
# the optimisation went there and did not like what it found.
# The fix is to set fAB to be -1 outside [0, 1], so the
# desired maximum is the global maximum.
#
# Note that we no longer use Brent, so this test is moot (but
# one never removes a regression test)

def test_small_line_segment():
    A = Ellipse(0, 0.1, np.pi/6)
    B = Ellipse(0.5, 1, 0)
    rAB = np.array([0, 0.3])
    res = sdpw_contact(A, B, rAB)
    assert res.maximum < np.inf
