from sdpw.ellipse import Ellipse
from sdpw.sdpw import fAB, fAB0
from pytest import approx
import numpy as np
import pytest


# the function fAB in the degenerate case

@pytest.fixture
def A():
    return Ellipse(1, 0, 0)


@pytest.fixture
def B():
    return Ellipse(1, 1, 0)


# fAB should be zero at 1.0

def test_disjoint(A, B):
    rAB = np.array([3, 0])
    assert fAB(A, B, rAB, 1.0) == approx(0)


def test_touching(A, B):
    rAB = np.array([2, 0])
    assert fAB(A, B, rAB, 1.0) == approx(0)


def test_intersection(A, B):
    rAB = np.array([1, 0])
    assert fAB(A, B, rAB, 1.0) == approx(0)


# fAB(A, B, rAB, x) -> fAB0(A, B, rAB) as x -> 0

def test_convergence(A, B):
    rAB = np.array([1, 0])
    ys = [fAB(A, B, rAB, x) for x in (0.1, 0.01, 0.001, 0.0001)]
    y0 = fAB0(A, B, rAB)
    d = [abs(y - y0) for y in ys]
    assert all(x >= y for x, y in zip(d, d[1:]))
