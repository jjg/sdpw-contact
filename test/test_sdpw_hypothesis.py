from sdpw.ellipse import Ellipse
from sdpw.sdpw import sdpw_contact, Maximum
from hypothesis import given, settings
from pytest import approx
import hypothesis.strategies as st
import pytest
import numpy as np


# These tests use Hypothesis, a property-based test generation
# system: https://hypothesis.works/

# rotation invariance, rotating both ellipses and the
# centre-vector by the same angle should result in the
# same contact value

def contact_rotated_by(theta, phi):
    A = Ellipse(1, 0, phi + theta)
    B = Ellipse(1, 2, theta)
    rAB = np.array([np.cos(theta), np.sin(theta)])
    return sdpw_contact(A, B, rAB)


@settings(max_examples=250)
@given(
    theta=st.floats(min_value=-2*np.pi, max_value=2*np.pi),
    phi=st.floats(min_value=-2*np.pi, max_value=2*np.pi)
)
def test_rotation_invariance(theta, phi):
    d0 = contact_rotated_by(0, phi)
    dt = contact_rotated_by(theta, phi)
    assert d0.maximum == approx(dt.maximum)


# check for successful evaluation for fixed line-segment
# and ellipse for variable orientaions and separation

def degenerate_case(x, y, s, t):
    A = Ellipse(1, 0, s)
    B = Ellipse(1, 2, t)
    rAB = np.array([x, y])
    res = sdpw_contact(A, B, rAB)
    assert isinstance(res, Maximum)
    assert isinstance(res.maximum, float)
    assert isinstance(res.maximiser, float)
    assert isinstance(res.iterations, int)
    assert res.maximum >= 0.0
    assert 0.0 <= res.maximiser <= 1.0
    assert res.iterations >= 0


@settings(max_examples=250)
@given(
    x=st.floats(min_value=-3, max_value=3),
    y=st.floats(min_value=-3, max_value=3),
    s=st.floats(min_value=-np.pi, max_value=np.pi),
    t=st.floats(min_value=-np.pi, max_value=np.pi)
)
def test_positive_on_coarse_mash(x, y, s, t):
    degenerate_case(x, y, s, t)


# these cases taken a similar but more extensive test in
# vfplot (witten in C), ported here for more-comfortable
# analysis environment, these have the optimisation
# objective with a small positive derivative at zero, and
# so the maximiser close to zero: numerical imprecision
# near the degeneracy is amplified as we approach it, and
# the objective can become non-convex, invalidating the
# optimisation.
#
# These now fixed with the Taylor quadratic approximation

def corner_case(x, y):
    degenerate_case(x, y, np.pi / 5, np.pi / 4)


def test_corner_case1():
    corner_case(-0.250000, 0.942000)


def test_corner_case2():
    corner_case(0.250000, -0.942000)


def test_corner_case3():
    corner_case(0.500000, -1.884000)
