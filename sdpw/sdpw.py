import numpy as np
import math


def qform(M, r):
    '''
    Evaluate a quadratic form
    '''
    return r @ M @ r.T


def grad_qform(M, r):
    '''
    The gradient of a quadratic form rMr
    '''
    return (M + M.T) @ r.T


def pdir(M, r):
    '''
    Principal direction of the quadratic form rMr, angle
    anticlockwise from the positive x-axis
    '''
    g = grad_qform(M, r)
    return math.atan2(g[1], g[0])


def C(A, B, x):
    return (1 - x) * A.M + x * B.M


def fAB(A, B, rAB, x):
    '''
    The Perram-Werheim objective
    '''
    D = np.linalg.inv(C(A, B, x))
    return x * (1 - x) * qform(D, rAB)


def dfAB(A, B, rAB, x):
    '''
    The derivative of the P-W objective
    '''
    D = np.linalg.inv(C(A, B, x))
    return qform(D @ ((1-x)**2 * A.M - x**2 * B.M) @ D, rAB)


def ddfAB(A, B, rAB, x):
    '''
    The second derivative of the P-W objective
    '''
    D = np.linalg.inv(C(A, B, x))
    return -qform(D @ (A.M @ D @ B.M + B.M @ D @ A.M) @ D, rAB)


def adj(M):
    a, b, c, d = M.flatten()
    return np.array([[d, -b],
                     [-c, a]])


def det(M):
    return np.linalg.det(M)


def D(A, B):
    return (adj(A.M) @ B.M).trace()


def fAB0(A, B, rAB):
    '''
    The limit, as x -> 0, of fAB(A, B, rAB, x) in the case that A is
    degenerate but not a point and B is non-degenerate
    '''
    return qform(adj(A.M), rAB) / D(A, B)


def dfAB0(A, B, rAB):
    '''
    The limit, as x -> 0, of of the derivative of fAB(A, B, rAB, x)
    with respect to x, in the case that A is degenerate but not a point
    and B is non-degenerate
    '''
    adjAM = adj(A.M)
    adjBM = adj(B.M)
    return qform(adjBM @ A.M @ adjBM - adjAM @ B.M @ adjAM, rAB) / D(A, B)**2


def ddfAB0(A, B, rAB):
    '''
    As dfAB0, but for the second derivative
    '''
    adjAM = adj(A.M)
    adjBM = adj(B.M)
    return -2 * det(B.M) * qform(adjBM @ A.M @ adjBM, rAB) / D(A, B)**3


class Maximum:
    '''
    A class for a return value for the contact functions: the extra data
    is for analysis and evaluation, generally one would only want to know
    the maximiser (or just that it is bigger than one).
    '''
    def __init__(self, t, fAB, theta, n):
        self.maximiser = float(t)
        self.maximum = float(fAB)
        self.theta = float(theta)
        self.iterations = int(n)


def initial_t(A, B):
    '''
    Initial value for the Newton iteration as suggested by A. Donev,
    S. Torquato, F. H. Stillinger, "Neighbor list collision-driven molecular
    dynamics simulation for nonspherical hard particles. II. Applications to
    ellipses and ellipsoids", J. Comp. Phys. 202 (2005) 765-793
    '''
    mA = float(max(A.a, A.b))
    mB = float(max(B.a, B.b))
    return mA / (mA + mB)


def pw_contact(A, B, rAB, t=None):
    if t is None:
        t = initial_t(A, B)
    return pw_contact_m(A.M, B.M, rAB, t)


def pw_contact_m(A, B, rAB, t0):
    '''
    The (non-degenerate) Perram-Wertheim contact function, maximised
    with a Newton-Raphson iteration to the zero of the derivative.
    '''
    t = t0
    for i in range(20):
        s = 1 - t
        s2 = s * s
        t2 = t * t

        D = np.linalg.inv(s * A + t * B)
        AD = A @ D
        BD = B @ D
        DAD = D @ AD
        DBD = D @ BD
        DADBD = DAD @ BD
        DBDAD = DBD @ AD

        fAB = s * t * qform(D, rAB)
        dfAB = s2 * qform(DAD, rAB) - t2 * qform(DBD, rAB)

        if abs(dfAB) < 1e-8:
            theta = pdir(D, rAB)
            return Maximum(t, fAB, theta, i + 1)

        ddfAB = -(qform(DADBD, rAB) + qform(DBDAD, rAB))
        if ddfAB == 0:
            raise RuntimeError('divide by zero')

        dt = dfAB / ddfAB
        t = constrained_subtract(t, dt)

    raise RuntimeError('too many iterations')


def constrained_subtract(t, dt):
    '''
    Here t is assumed in [0, 1], and we want to subtract dt
    from it; if that takes us outside [0, 1] then we instead
    move halfway towards the boundary. One could probably do
    better than this.
    '''
    t1 = t - dt
    if t1 <= 0:
        return t / 2
    if t1 >= 1:
        return (t + 1) / 2
    return t1


def quadratic(a, b, c):
    def q(x):
        return (a * x + b) * x + c
    return q


def sdquad_max(f0, df0):
    '''
    The maximiser for the quadratic which has the same value and
    derivative of f at zero, and which is zero at one.  This is
    useful as a string point for the non-degenerate PW-iteration,
    using it (rather than 0.5) results to a small reduction in the
    number of Newton-Raphson iterations for most geometries tested.
    '''
    b = df0
    c = f0
    a = -(b + c)
    if a == 0:
        x0 = 0.5
    else:
        x0 = -b / (2 * a)
    return x0, quadratic(a, b, c)


def taylor_max(f0, df0, ddf0):
    '''
    Maximiser for the Taylor series of f at 0 truncated to a
    quadratic, if this is close to zero then we use it instead
    of the non-degenerate PW
    '''
    a = ddf0 / 2
    b = df0
    c = f0
    x0 = -b / (2 * a)
    return x0, quadratic(a, b, c)


def sdpw_contact(A, B, rAB):
    '''
    The semi-degenerate Perram-Wertheim contact function.
    '''
    f0 = fAB0(A, B, rAB)
    df0 = dfAB0(A, B, rAB)

    if df0 <= 0:
        theta = pdir(adj(A.M), rAB)
        return Maximum(0, f0, theta, 0)

    ddf0 = ddfAB0(A, B, rAB)

    xm, fm = taylor_max(f0, df0, ddf0)
    if xm < 0.01:
        theta = pdir(adj(A.M), rAB)
        return Maximum(xm, fm(xm), theta, 0)

    xm, _ = sdquad_max(f0, df0)

    return pw_contact(A, B, rAB, xm)
