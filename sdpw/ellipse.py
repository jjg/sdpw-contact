"""

.. class:: Ellipse
   :synopsis: Minimal ellipse objects

.. moduleauthor:: J.J. Green <j.j.green@gmx.co.uk>

"""

import numpy as np


class Ellipse:

    def __init__(self, a, b, theta):
        self.a = a
        self.b = b
        self.theta = theta
        ct = np.cos(theta)
        st = np.sin(theta)
        self.radius = max(a, b)
        R = np.array([[ct, -st], [st, ct]])
        self.M = R @ np.array([[a * a, 0], [0, b * b]]) @ R.T
