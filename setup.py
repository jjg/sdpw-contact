from setuptools import setup

setup(
    name = 'sdpw-contact',
    version = '0.1.0',
    description = 'Semi-degenerate Perram-Wertheim contact function',
    url = 'https://jjg.gitlab.io/',
    author = 'J. J. Green',
    author_email = 'j.j.green@gmx.co.uk',
    license = 'LICENSE.txt',
    packages = ['sdpw'],
    keywords = ['ellipse', 'ellipsoid', 'Perram-Wertheim'],
    zip_safe = True,
    classifiers = [
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: Console',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: MIT License (MIT)',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Operating System :: OS Independent',
        'Topic :: Scientific/Engineering :: Mathematics'
    ],
    install_requires = ['numpy']
)
