#!/usr/bin/env python3
#
# Create a data for plots of fAB fot A fixed and B with
# variable ellipticity: to illustrate behaviour as we
# approach degeneracy

import os
import sys
sys.path.insert(0, os.path.abspath('../..'))

import numpy as np

from sdpw.ellipse import Ellipse
from sdpw.sdpw import fAB, fAB0

ellipticities = [4, 8, 16, 32, 64, 128]

B = Ellipse(0.5, 1, 0)
rAB = np.array([0.3, 0])
alpha = np.pi/4
n = 300

for ellipticity in ellipticities:

    filename = "fAB%03i.dat" % ellipticity
    print(filename)

    A = Ellipse(1.0 / ellipticity, 1, alpha)

    f = open(filename, 'w')
    for x in np.linspace(1, 0, num=n, endpoint=True):
        f.write("%.8f %.8f\n" % (x, fAB(A, B, rAB, x)))
    f.close()

filename = "fABinf.dat"
print(filename)

A = Ellipse(0, 1, alpha)

f = open(filename, 'w')
for x in np.linspace(1, 0, num=n-1, endpoint=False):
    f.write("%.8f %.8f\n" % (x, fAB(A, B, rAB, x)))
f.write("%.8f %.8f\n" % (0, fAB0(A, B, rAB)))
f.close()
