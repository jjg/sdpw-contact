Example plots
-------------

The _sdpw_ code is used to generate various pdf plots using
the Generic Mapping Tools (GMT) 5+.  The git achive for this
project does not include the pdf plots, but does have some
thumbnails.
