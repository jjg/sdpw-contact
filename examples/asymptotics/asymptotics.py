#!/usr/bin/env python3
#
# Plot of fAB in the degenerate case, along with
# Taylor expansions (linear, quadratic) about zero.

import os
import sys
sys.path.insert(0, os.path.abspath('../..'))

import numpy as np

from sdpw.ellipse import Ellipse
from sdpw.sdpw import fAB, fAB0, dfAB0, ddfAB0

n = 300

B = Ellipse(0.5, 1, 0)
rAB = np.array([0.3, 0])
alpha = np.pi/4

A = Ellipse(0, 1, alpha)

y0 = fAB0(A, B, rAB)
dy0 = dfAB0(A, B, rAB)
ddy0 = ddfAB0(A, B, rAB)

filename = "fAB.dat"
print(filename)

f = open(filename, 'w')
for x in np.linspace(1, 0, num=n-1, endpoint=False):
    f.write("%.8f %.8f\n" % (x, fAB(A, B, rAB, x)))
f.write("%.8f %.8f\n" % (0, y0))
f.close()

# linear Taylor

filename = "fAB1.dat"
print(filename)

f = open(filename, 'w')
for x in np.linspace(1, 0, num=n-1, endpoint=False):
    f.write("%.8f %.8f\n" % (x, y0 + x * dy0))
f.write("%.8f %.8f\n" % (0, y0))
f.close()

# quadratic Taylor

filename = "fAB2.dat"
print(filename)

f = open(filename, 'w')
for x in np.linspace(1, 0, num=n-1, endpoint=False):
    f.write("%.8f %.8f\n" % (x, y0 + x * dy0 - 0.5 * x * x * ddy0))
f.write("%.8f %.8f\n" % (0, y0))
f.close()
