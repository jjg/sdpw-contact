asymptotics.pdf
-------------------

Plot of _fAB_ in the degenerate case, along with the Taylor
expansions (linear, quadratic) about zero.

![thumbnail](asymptotics.png)
