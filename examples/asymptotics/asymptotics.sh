#!/bin/sh

RNG='-R0/1/0/0.15'
PRJ='-JX3i/3i'

A1PEN='-W0.75p,grey'
A2PEN='-W0.75p,red'
LPEN='-W1.50p,black'

PS='asymptotics.ps'

gmt psxy fAB2.dat $A2PEN $PRJ $RNG -K > $PS
gmt psxy fAB1.dat $A1PEN $PRJ $RNG -K -O >> $PS
gmt psxy fAB.dat $LPEN $PRJ $RNG -K -O >> $PS
gmt psbasemap -Bfa0.2/0.01neWS $RNG $PRJ -O >> $PS
gmt psconvert -A -P -Tf $PS
rm $PS
