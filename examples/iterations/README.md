Iterations
----------

The number of iterations (blue 0, red 18) of Newton-Raphson
iteration for the (semi-degenerate) Perram-Wertheim distance,
the geometries as for the [potential](../potential) plots;
contributions from more than one line-segment are summed.

![thumbnail](corner.png)
![thumbnail](koch.png)
![thumbnail](rod.png)
