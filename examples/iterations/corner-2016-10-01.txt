corner.grd: Title: corner.grd
corner.grd: Command: xyz2grd /tmp/sdpw-corner.dat -Gcorner.grd -I400+/400+ -R-1.00000000/1.00000000/-1.00000000/1.00000000
corner.grd: Remark: 
corner.grd: Gridline node registration used [Cartesian grid]
corner.grd: Grid file format: nf = GMT netCDF format (32-bit float), COARDS, CF-1.5
corner.grd: x_min: -1 x_max: 1 x_inc: 0.00501253132832 name: x nx: 400
corner.grd: y_min: -1 y_max: 1 y_inc: 0.00501253132832 name: y ny: 400
corner.grd: z_min: 0 z_max: 16 name: z
corner.grd: scale_factor: 1 add_offset: 0
corner.grd: 81239 nodes (50.8%) set to NaN
corner.grd: mean: 4.17836238748 stdev: 3.95823543964 rms: 5.7555313491
corner.grd: format: netCDF-4 chunk_size: 134,134 shuffle: on deflation_level: 3
