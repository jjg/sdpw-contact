#!/bin/sh

set -e

BASE=$1

RNG='-R-1/1/-1/1'
PRJ='-JX3i/3i'

LPEN='-W1.50p,black'
WPEN='-W3p,white'

PS="$BASE.ps"
GRD="$BASE.grd"

# iteration colours

gmt grdimage $GRD -Q -Citerations $RNG $PRJ -K > $PS

# the boundary

gmt psxy $BASE-lines.dat $WPEN $PRJ $RNG -K -O >> $PS
gmt psxy $BASE-lines.dat $LPEN $PRJ $RNG -K -O >> $PS

# basemap

gmt psbasemap -B0 $RNG $PRJ -O >> $PS

# output

gmt psconvert -A -P -Tf $PS
rm $PS
