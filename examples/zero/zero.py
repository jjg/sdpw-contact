#!/usr/bin/env python3
#
# this creates a mesh of distance values for a contour
# plot illustrating the Perram-Wertheim distance between
# an ellipse and a line segment; output to be meshed by
# GMT's xyz2grd

import os
import sys
sys.path.insert(0, os.path.abspath('../..'))

from sdpw.ellipse import Ellipse
from sdpw.sdpw import dfAB0

import numpy as np

xmin = -1.5
xmax =  2
ymin = -0.5
ymax =  1

nx = 700
ny = 300

A = Ellipse(1, 0, 0)
B = Ellipse(0.5, 0.25, np.pi/4)

for x in np.linspace(xmin, xmax, num=nx, endpoint=True):
    for y in np.linspace(ymin, ymax, num=ny, endpoint=True):
        rAB = np.array([x, y])
        d = dfAB0(A, B, rAB)
        print("%.8f %.8f %.8f" % (x, y, d))
