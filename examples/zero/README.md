zero.pdf
--------

A contour plot of the derivative of the function fAB evaluated
at zero.  The line A is considered fixed, the ellipse B is free
to move on the plane.

![thumbnail](zero.png)

We remark the the negativity of this derivative when the
non-degenerate ellipse is located aside the degenarate bodes
well for the "boundary application" since most ellipses so
bound will be in that location, and the numerics in this case is
trivial, the maximum is at zero (only fAB(0) and fAB'(0) need be
calculated, no iteration is needed).
