#!/bin/sh

RNG='-R-1.5/2/-0.5/1'
PRJ='-JX14c/6c'

CPEN='-W0.75p,grey'
APEN='-W0.75p,black'
LPEN='-W1.50p,black'
WPEN='-W3p,white'

PS="zero.ps"

# grey contour lines, not annoted

gmt grdcontour zero.grd $CPEN -C0.2 $RNG $PRJ -K > $PS

# heavier black contours, annoted

ANLINE=-Gl-1.5/0.2/0.6/1
ANTEXT=-A1+gwhite+c1p/0

gmt grdcontour zero.grd $APEN $ANLINE $ANTEXT $RNG $PRJ -K -O >> $PS

# the line and ellipse: first in thick white ...

gmt psxy lines.dat $WPEN $PRJ $RNG -K -O >> $PS
gmt psxy ellipses.dat -Se $WPEN $PRJ $RNG -K -O >> $PS

# ... then in regular-weight black

gmt psxy lines.dat $LPEN $PRJ $RNG -K -O >> $PS
gmt psxy ellipses.dat -Se $LPEN $PRJ $RNG -K -O >> $PS

# the centre dots for the line and ellipse

gmt psxy dots.dat -Sc4p -Gblack $PRJ $RNG -K -O >> $PS

# labels on the dots ("A" and "B")

DOTFONT="16,Times-Italic"
DOTOUTLINE="$DOTFONT,-=2p,white"

gmt pstext dots.dat -Dj4p -F+a0+f$DOTOUTLINE+j $PRJ $RNG -K -O >> $PS
gmt pstext dots.dat -Dj4p -F+a0+f$DOTFONT+j $PRJ $RNG -K -O >> $PS

# basemap

gmt psbasemap -Bfa0.5neWS $RNG $PRJ -O >> $PS

# convert to pdf

gmt psconvert -C-dCompatibilityLevel=1.5 -A -P -Tf $PS

# cleanup

rm $PS
