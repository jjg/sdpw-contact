contour.pdf
-----------

Scripts and data for a plot of the Perram-Wertheim distance
between a fixed line A, [-1, 1] on the x-asis, and an ellipse
B, free to move on the plane.

![Thumbnail](contour.png)
