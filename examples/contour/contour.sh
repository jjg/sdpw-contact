#!/bin/sh

set -e

RNG='-R-1.5/2/-0.5/1'
PRJ='-JX14c/6c'

CPEN='-W0.75p,grey'
APEN='-W0.75p,black'
LPEN='-W1.50p,black'
WPEN='-W3p,white'

PS='contour.ps'

# contours

gmt grdcontour contour.grd $CPEN -C0.2 $RNG $PRJ -K > $PS
gmt grdcontour contour.grd $APEN -C1 $RNG $PRJ -K -O >> $PS

# contour annotation

ANFONT="8,Courier-Bold"
ANOUTLINE="$ANFONT,-=2.0p,white"

LABELS='/tmp/sdpw-contour-labels.dat'
gmt grdcontour contour.grd -Gl0/-2/0/2 -A1+t$LABELS $RNG $PRJ > /dev/null
gmt pstext $LABELS -Dj4p -F+a+f$ANOUTLINE+jMC $PRJ $RNG -K -O >> $PS
gmt pstext $LABELS -Dj4p -F+a+f$ANFONT+jMC $PRJ $RNG -K -O >> $PS
rm $LABELS

# rest of the geometry

gmt psxy lines.dat $WPEN $PRJ $RNG -K -O >> $PS
gmt psxy ellipses.dat -Se $WPEN $PRJ $RNG -K -O >> $PS
gmt psxy lines.dat $LPEN $PRJ $RNG -K -O >> $PS
gmt psxy ellipses.dat -Se $LPEN $PRJ $RNG -K -O >> $PS
gmt psxy dots.dat -Sc4p -Gblack $PRJ $RNG -K -O >> $PS

# labels

DOTFONT="16,Times-Italic"
DOTOUTLINE="$DOTFONT,-=2p,white"

gmt pstext dots.dat -Dj4p -F+a0+f$DOTOUTLINE+j $PRJ $RNG -K -O >> $PS
gmt pstext dots.dat -Dj4p -F+a0+f$DOTFONT+j $PRJ $RNG -K -O >> $PS

# basemap

gmt psbasemap -Bfa0.5neWS $RNG $PRJ -O >> $PS

gmt psconvert -C-dCompatibilityLevel=1.5 -A -P -Tf $PS
rm $PS
