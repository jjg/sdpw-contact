# generate a bit of the koch snowflake, in csv format

import numpy as np

scale = 2
xshift = -1
yshift = -0.2
depth = 2

pts = np.array([0,], dtype=complex)

s32 = np.sqrt(3)/2

cw = 0.5 - s32 * 1j
ccw = 0.5 + s32 * 1j

X = 1./3 + 0j
Y = 1./2 + s32/3 * 1j
Z = 2./3 + 0j

for n in range(depth):
    a = pts / 3
    b = ccw * a + X
    c = cw * a + Y
    d = a + Z
    pts = np.concatenate([a, b, c, d])

pts = np.append(pts, 1) * scale + (xshift + yshift * 1j)

coords = ((z.real, z.imag) for z in pts)

for coord in coords:
    print "%f,%f" % coord
