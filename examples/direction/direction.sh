#!/bin/sh

set -e

BASE=$1
ALINE=$2

RNG='-R-1/1/-1/1'
PRJ='-JX3i/3i'

CPEN='-W0.75p,grey'
APEN='-W0.75p,black'
LPEN='-W1.50p,black'
WPEN='-W3p,white'

PS="$BASE.ps"

DOTFONT="7,Courier-Bold"
DOTOUTLINE="$DOTFONT,-=2.5p,white"

# arrows

gmt grdvector ${BASE}x.grd ${BASE}y.grd \
    -Ix4 \
    -S0.20p \
    -Q2p+e+jc+n0.05p \
    -Gblack \
    $RNG $PRJ -K > $PS

# the boundary

gmt psxy $BASE-lines.dat $WPEN $PRJ $RNG -K -O >> $PS
gmt psxy $BASE-lines.dat $LPEN $PRJ $RNG -K -O >> $PS

# basemap

gmt psbasemap -B0 $RNG $PRJ -O >> $PS

# output

gmt psconvert -A -P -Tf $PS
rm $PS
