direction
---------

A Lennard-Jones style potential based on the Perram-Wertiem
distance is used to generate force vectors, these are summed
for a series of line-segments.

In the _corner_ plot

![thumbnail](corner.png)

the segments are of about the same size as the non-degenerate
ellipse, arranged to make a corner.

In the _koch_ plot

![thumbnail](koch.png)

the segments are rather smaller relative to the non-degenerate
ellipse, and arranged in a (very) partial Koch snowflake.

For the _rod_ plot

![thumbnail](rod.png)

four line-segments are arranged to make a rod.
