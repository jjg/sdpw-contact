#!/usr/bin/env python3
#
# This creates a potential in the manner of Lennard-Jones,
# the potential is simple function of the Perram-Werheim
# contact function.  It reads a CSV file of corners of a
# piecewise linear path and inteprets the segments as
# degenerate ellipses.
#
# This is quite slow..

import os
import sys
sys.path.insert(0, os.path.abspath('../..'))

import csv

from sdpw.ellipse import Ellipse
from sdpw.sdpw import sdpw_contact

import numpy as np
from numpy.linalg import norm
from math import atan2

from tempfile import NamedTemporaryFile

xmin = -1
xmax =  1
ymin = -1
ymax =  1

nx = 400
ny = 400

lib = sys.argv[1]
base = sys.argv[2]

def csv_file(name):
    return "%s/%s.csv" % (lib, name)

print('processing %s' % base)

# the lines

print("setting up lines")

def vector(x, y):
    return np.array([x, y])

ps = []

with open(csv_file(base), 'r') as f:
    rows = csv.reader(f)
    for row in rows:
        frow = map(float, row)
        ps.append(vector(*frow))

pairs = zip(ps[:-1], ps[1:])

lines = []

for pair in pairs:
    p, q = pair
    v = q - p
    c = 0.5 * (p + q)
    m = norm(v, 2)
    t = atan2(v[1], v[0])
    A = Ellipse(0, m/2, t + np.pi/2)
    lines.append( (A, c) )

fd = open('%s-lines.dat' % base, "w")
for p in ps:
    fd.write("%.8f %.8f\n" % (p[0], p[1]))
fd.close()

# the mesh of the potential

B = Ellipse(0.3, 0.3, np.pi/2)

def LJ(x):
    if x > 1:
        return 0
    return 1 - x

print("creating grid data")

fd = NamedTemporaryFile(mode='w')
table = fd.name

for x in np.linspace(xmin, xmax, num=nx, endpoint=True):
    for y in np.linspace(ymin, ymax, num=ny, endpoint=True):
        v = vector(x, y)
        K = 0.0
        for line in lines:
            A, c = line
            rAB = v - c
            # this cheap check, that the two ellipses are disjoint
            # since their minimal bounding circles have centres
            # sufficiently apart, reduces the time for the corner
            # plot data generation from 5m30s to 50s
            if norm(rAB) <= A.radius + B.radius:
                res = sdpw_contact(A, B, rAB)
                K += LJ(res.maximum)
        fd.write("%.8f %.8f %.8f\n" % (x, y, K))

fd.flush()
os.fsync(fd)

print("gridding")

cmd = " ".join(['gmt',
                'xyz2grd',
                table,
                '-G%s.grd' % base,
                '-I%i+/%i+' % (nx, ny),
                '-R%.8f/%.8f/%.8f/%.8f' % (xmin, xmax, ymin, ymax)])

os.system(cmd)
fd.close()

print("done.")
