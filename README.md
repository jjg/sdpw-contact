sdpw-contact
------------

A Python module implementing an algorithm [1] extending the Perram-Wertheim
contact function of a pair of ellipses (or higher-dimensional ellipsoids)
[2] to the case where one of the ellipses is degenerate but nonzero.

This code is _not_ a reference implementation, it aims to be clear and
obvious at the cost of efficiency, and to generate some plots to be used
in [1].

1. J. J. Green, "The Perram-Wertheim contact-function in the semi-degenerate
   case" (in preparation)

2. John W. Perram, M. S. Wertheim, "Statistical mechanics of hard ellipsoids.
   I. Overlap algorithm and the contact function", J. Comp. Phys. 58 (3),
   1985, 409--416
